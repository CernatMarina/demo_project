package com.example.marinacernat.proiectdemo;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SetPassword extends AppCompatActivity
{

    EditText Password;
    EditText ConfirmPassword;
    Button NextButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_password);

        NextButton = (Button) findViewById(R.id.next_btn);

        NextButton.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View v) {

                Bundle MainActivityData = getIntent().getExtras();
                if(MainActivityData == null)
                {
                    return;
                }

                String phone = MainActivityData.getString("phone");
                Password = (EditText) findViewById(R.id.et_Password);
                ConfirmPassword = (EditText) findViewById(R.id.et_ConfirmPassword);

                // posibil sa fie nevoie de rescrierea lui equals
                if( (Password.getText().toString()).equals(ConfirmPassword.getText().toString()) == false || (Password.getText().toString().length() < 8) || (ConfirmPassword.getText().toString().length() < 8) )
                {
                    Toast.makeText(getApplicationContext(), "Parolele nu coincid sau au mai putin de 8 caractere", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Intent i = new Intent(getApplicationContext(), FillInProfileInformation.class);
                    i.putExtra("phone", phone);
                    i.putExtra("Password", Password.getText().toString());
                    startActivity(i);
                }
            }
        });

    }




}
