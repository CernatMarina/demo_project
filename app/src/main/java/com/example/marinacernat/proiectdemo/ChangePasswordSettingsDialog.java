package com.example.marinacernat.proiectdemo;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.marinacernat.proiectdemo.R;

import static com.facebook.FacebookSdk.getApplicationContext;

public class ChangePasswordSettingsDialog extends AppCompatDialogFragment {

    private EditText password_edit_text, confirm_password_edit_text;

    private ChangePasswordDialogListener listener;

    //Button cancel_modif_password, save_password_changes;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_change_password, null);

        password_edit_text = (EditText) view.findViewById(R.id.password_edit_text);
        confirm_password_edit_text = (EditText) view.findViewById(R.id.confirm_password_edit_text);

       // cancel_modif_password = (Button) view.findViewById(R.id.cancel_modif_password);
      // save_password_changes= (Button) view.findViewById(R.id.save_password_changes);

         builder.setView(view)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .setPositiveButton("Change Password", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                    String changed_password = password_edit_text.getText().toString();
                    String confirm_changed_password = confirm_password_edit_text.getText().toString();

                     if(changed_password.length()<8 || confirm_changed_password.length()<8)
                     {
                         Toast.makeText(getApplicationContext(), "Parola trebuie sa aiba minim 8 caractere", Toast.LENGTH_SHORT).show();
                     }
                     else
                     {
                         if(!changed_password.equals(confirm_changed_password))
                        {
                             Toast.makeText(getApplicationContext(), "Parolele nu sunt identice", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                          //.applyText(changed_password);

                            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("newPassword", changed_password);
                            editor.apply();

                            Toast.makeText(getApplicationContext(), "Password changed", Toast.LENGTH_SHORT).show();
                        }
                     }

                 }
               });


     /*   cancel_modif_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        save_password_changes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });  */

        return builder.create();

    }

 /*   @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            listener = (ChangePasswordDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "must implement ChangePasswordDialogListener");
        }
    }   */

    public interface ChangePasswordDialogListener{
        void applyText(String changed_password);
    }
}
