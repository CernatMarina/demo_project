package com.example.marinacernat.proiectdemo;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.squareup.picasso.Picasso;

import static com.facebook.FacebookSdk.getApplicationContext;


/**
 * A simple {@link Fragment} subclass.
 */
public class DetaliiOferte extends Fragment {

    private String title_bundle;
    private String poster_path_bundle;
    private String overview_bundle;
    private String vote_count_bundle;

    private TextView title;
    private ImageView poster_path;
    private TextView overview;
    private TextView vote_count;

    private Button item_video;
    private VideoView videoView;

    private Button webPage;

    View v;

    public DetaliiOferte() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d("titlu",  getArguments().getString("title"));

        title_bundle = getArguments().getString("title");
        poster_path_bundle= getArguments().getString("poster_path");
        overview_bundle = getArguments().getString("overview");
        vote_count_bundle = getArguments().getString("vote_count");


        v = inflater.inflate(R.layout.fragment_detalii_oferte, container, false);

        title = (TextView) v.findViewById(R.id.movie_title);
        poster_path = (ImageView) v.findViewById(R.id.item_poster_path);
        overview = (TextView) v.findViewById(R.id.item_overview);
        vote_count = (TextView) v.findViewById(R.id.item_vote_count);


        title.setText(title_bundle);
        Picasso.with(getApplicationContext()).load(poster_path_bundle).into(poster_path);
        overview.setText(overview_bundle);
        vote_count.setText(vote_count_bundle);


        item_video = v.findViewById(R.id.item_video);
        videoView = v.findViewById(R.id.videoView_id);


        item_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                poster_path.setVisibility(View.GONE);
                videoView.setVisibility(View.VISIBLE);
            }
        });


        Uri uri = Uri.parse("android.resource://"+getActivity().getPackageName()+"/"+R.raw.video);
        videoView.setVideoURI(uri);
        videoView.start();


        webPage = v.findViewById(R.id.item_web_page);

        webPage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("https://en.wikipedia.org/wiki/" + title_bundle));
                startActivity(intent);
            }
        });

        return v;
    }

}
