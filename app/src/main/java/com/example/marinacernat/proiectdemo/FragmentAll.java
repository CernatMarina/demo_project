package com.example.marinacernat.proiectdemo;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.example.marinacernat.proiectdemo.api.model.Movie;
import com.example.marinacernat.proiectdemo.api.response.GetMoviesResponse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;

public class FragmentAll extends Fragment {

    View v;
    private RecyclerView myrecyclerview;
    private List<Movie> lstMovie;
    private RecyclerViewAdapter recyclerAdapter;


    public FragmentAll() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_fragment_all, container, false);



        myrecyclerview = (RecyclerView) v.findViewById(R.id.all_recyclerview);

        recyclerAdapter = new RecyclerViewAdapter(getContext(), getActivity().getSupportFragmentManager());


        GridLayoutManager myGrid = new GridLayoutManager(getActivity(), 2);

        myGrid.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup(){
            @Override
            public int getSpanSize(int position) {
                if(position%3 == 0)
                    return 2;
                else
                    return 1;
            }
        });


        myrecyclerview.setLayoutManager(myGrid);
        myrecyclerview.setAdapter(recyclerAdapter);

        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume()
    {
        super.onResume();

        getMoviesList();
    }

    private void getMoviesList()
    {
        Call<GetMoviesResponse> responseCall = ((SampleApp) getActivity().getApplication()).getWebServices().getMovies(getString(R.string.api_key));

        responseCall.enqueue(new Callback<GetMoviesResponse>() {
            @Override
            public void onResponse(Call<GetMoviesResponse> call, Response<GetMoviesResponse> response) {
                GetMoviesResponse getMoviesResponse = response.body();

                lstMovie = new ArrayList<>();

                lstMovie = getMoviesResponse.getResults();

                int nr = lstMovie.size();

                String movieNames = new String();

                for(int i =0; i<nr; i++)
                {
                    movieNames = movieNames + lstMovie.get(i).getTitle() + "   ";
                }

                Log.d("Lista titlurilor:", movieNames);

                recyclerAdapter.updateMoviesList(lstMovie);

            }

            @Override
            public void onFailure(Call<GetMoviesResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Network error occured", Toast.LENGTH_SHORT).show();

            }
        });
    }

}
