package com.example.marinacernat.proiectdemo;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import static com.example.marinacernat.proiectdemo.MainActivity.myAppDatabase;
import static com.facebook.FacebookSdk.getApplicationContext;
import static com.facebook.login.widget.ProfilePictureView.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileSettings extends Fragment{

    EditText phone;
    EditText first_name;
    EditText last_name;
    EditText birth_day;
    EditText email;
    TextView password;
    ImageView user_picture;

    Button female1, male1, female2, male2, save, log_out, show_change_password, settings_back_button;

    private EditText password_edit_text, confirm_password_edit_text;

    String phone_number;
    String facebook_id;
    String gender;

    String new_password;

    FragmentManager fm;

    View v;

    public ProfileSettings() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_profile_settings, container, false);

        settings_back_button = (Button) v.findViewById(R.id.settings_back_button);

        phone = (EditText) v.findViewById(R.id.settings_phone_edit);
        first_name = (EditText) v.findViewById(R.id.settings_first_name_edit);
        last_name = (EditText) v.findViewById(R.id.settings_last_name_edit);
        birth_day = (EditText) v.findViewById(R.id.settings_birth_day_edit);
        email = (EditText) v.findViewById(R.id.settings_email_edit);
        password = (TextView) v.findViewById(R.id.settings_password);
        user_picture=(ImageView) v.findViewById(R.id.image_profile);

        female1 = (Button) v.findViewById(R.id.button_female1);
        male1 = (Button) v.findViewById(R.id.button_male1);
        female2 =(Button) v.findViewById(R.id.button_female2);
        male2 =  (Button) v.findViewById(R.id.button_male2);

        save=(Button) v.findViewById(R.id.save_changes);
        log_out=(Button) v.findViewById(R.id.log_out);
        show_change_password=(Button) v.findViewById(R.id.show_change_password);


        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences("userInfo", Context.MODE_PRIVATE);

        if(sharedPreferences.getString("logged", "").equals("true"))
        {
            Log.d("numar de tel",  sharedPreferences.getString("user_phone", ""));

            phone_number = sharedPreferences.getString("user_phone", "");

            new SearchProfileSettingsAsyncTask(phone_number, new FragmentProfileSettingsTaskDelegate(){

                @Override
                public void onFinishedFragmentProfileSettingsTask() {

                    if(facebook_id != null)
                    {
                        new ProfileSettings.GetFbProfilePicture(new ProfileSettings.FragmentProfileSettingsTaskDelegate(){

                            @Override
                            public void onFinishedFragmentProfileSettingsTask() {

                                settings_back_button.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                                        fragmentTransaction.replace(R.id.main_frame, new ProfileFragment()); // id-ul frame-layout-ului si id-ul fragmentului pe care vrem sa il punem
                                        fragmentTransaction.addToBackStack(null);
                                        fragmentTransaction.commit();
                                    }
                                });

                                show_change_password.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        openDialog().show();
                                       // getNewPassword();
                                    }
                                });

                                log_out.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                                        SharedPreferences.Editor editor = sharedPreferences.edit();
                                        editor.putString("logged", "false");
                                        editor.putString("user_phone", phone_number);
                                        editor.apply();

                                        Intent i = new Intent(getActivity(), MainActivity.class);
                                        startActivity(i);

                                    }
                                });

                                female1.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        male1.setVisibility(View.VISIBLE);
                                        female1.setVisibility(View.VISIBLE);

                                        male2.setVisibility(View.GONE);
                                        female2.setVisibility(View.GONE);

                                        gender = "female";
                                    }
                                });

                                male1.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        male2.setVisibility(View.VISIBLE);
                                        female2.setVisibility(View.VISIBLE);

                                        male1.setVisibility(View.GONE);
                                        female1.setVisibility(View.GONE);
                                        gender = "male";
                                    }
                                });


                                female2.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        male1.setVisibility(View.VISIBLE);
                                        female1.setVisibility(View.VISIBLE);

                                        male2.setVisibility(View.GONE);
                                        female2.setVisibility(View.GONE);

                                        gender = "female";
                                    }
                                });

                                male2.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        male2.setVisibility(View.VISIBLE);
                                        female2.setVisibility(View.VISIBLE);

                                        male1.setVisibility(View.GONE);
                                        female1.setVisibility(View.GONE);
                                        gender = "male";
                                    }
                                });

                                save.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        new UpdateProfileSettingsAsyncTask(phone_number, gender, new FragmentProfileSettingsTaskDelegate() {

                                            @Override
                                            public void onFinishedFragmentProfileSettingsTask() {

                                            }
                                        }).execute();
                                    }
                                });

                            }
                        }).execute();
                    }

                }
            }).execute();
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Nu sunteti logat", Toast.LENGTH_SHORT).show();
        }



        return v;
    }


    public class SearchProfileSettingsAsyncTask extends AsyncTask<Void, Void, List<UserEntity>> {

        String phone_nr;
        ProfileSettings.FragmentProfileSettingsTaskDelegate taskDelegate;

        SearchProfileSettingsAsyncTask(String phone, ProfileSettings.FragmentProfileSettingsTaskDelegate taskDelegate){
            this.phone_nr=phone;
            this.taskDelegate = taskDelegate;
        }


        @Override
        protected List<UserEntity> doInBackground(Void... taskDelegates) {
            List<UserEntity> users = myAppDatabase.myDao().getUsersByPhone(phone_nr);
            return users;
        }

        @Override
        protected void onPostExecute(List<UserEntity> users) {
            super.onPostExecute(users);

            facebook_id = users.get(0).getFacebook_id();

            phone.setText(phone_number);
            first_name.setText(users.get(0).getFirstName());
            last_name.setText(users.get(0).getLastName());
            birth_day.setText(users.get(0).getBirthday());
            email.setText(users.get(0).getEmail());
            password.setText(users.get(0).getPassword());

            if(users.get(0).getGender()!=null && users.get(0).getGender().equals("male"))
            {
               male2.setVisibility(View.VISIBLE);
               female2.setVisibility(View.VISIBLE);

                male1.setVisibility(View.GONE);
                female1.setVisibility(View.GONE);
                gender = "male";
            }

            else
            {
                male1.setVisibility(View.VISIBLE);
                female1.setVisibility(View.VISIBLE);

                male2.setVisibility(View.GONE);
                female2.setVisibility(View.GONE);
                gender = "female";
            }

            if(taskDelegate!=null){
                taskDelegate.onFinishedFragmentProfileSettingsTask();
            }
        }
    };


    public class GetFbProfilePicture extends AsyncTask<Void, Void, Bitmap> {

        ProfileSettings.FragmentProfileSettingsTaskDelegate taskDelegate;

        GetFbProfilePicture(ProfileSettings.FragmentProfileSettingsTaskDelegate taskDelegate){

            this.taskDelegate = taskDelegate;
        }


        @Override
        protected Bitmap doInBackground(Void... taskDelegates) {


            URL img_value = null;

            try {
                img_value = new URL("https://graph.facebook.com/"+facebook_id+"/picture?type=large");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            Bitmap mIcon1 = null;
            try {
                mIcon1 = BitmapFactory.decodeStream(img_value.openConnection().getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }

            return mIcon1;
        }

        @Override
        protected void onPostExecute(Bitmap mIcon1) {
            super.onPostExecute(mIcon1);

            user_picture.setImageBitmap(mIcon1);

            if(taskDelegate!=null){
                taskDelegate.onFinishedFragmentProfileSettingsTask();
            }
        }
    };

    public Dialog openDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_change_password, null);

        password_edit_text = (EditText) view.findViewById(R.id.password_edit_text);
        confirm_password_edit_text = (EditText) view.findViewById(R.id.confirm_password_edit_text);

        builder.setView(view)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .setPositiveButton("Change Password", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        String changed_password = password_edit_text.getText().toString();
                        String confirm_changed_password = confirm_password_edit_text.getText().toString();

                        if(changed_password.length()<8 || confirm_changed_password.length()<8)
                        {
                            Toast.makeText(getApplicationContext(), "Parola trebuie sa aiba minim 8 caractere", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            if(!changed_password.equals(confirm_changed_password))
                            {
                                Toast.makeText(getApplicationContext(), "Parolele nu sunt identice", Toast.LENGTH_SHORT).show();
                            }
                            else
                            {
                                new_password = changed_password;
                                Toast.makeText(getApplicationContext(), "Password changed", Toast.LENGTH_SHORT).show();
                            }
                        }

                    }
                });

        return builder.create();
    }


    public class UpdateProfileSettingsAsyncTask extends AsyncTask<Void, Void, List<UserEntity>> {

        String phone_nr;
        String gender;
        ProfileSettings.FragmentProfileSettingsTaskDelegate taskDelegate;

        UpdateProfileSettingsAsyncTask(String phone, String gender, ProfileSettings.FragmentProfileSettingsTaskDelegate taskDelegate){
            this.phone_nr=phone;
            this.gender=gender;
            this.taskDelegate = taskDelegate;
        }


        @Override
        protected List<UserEntity> doInBackground(Void... taskDelegates) {
            List<UserEntity> users = myAppDatabase.myDao().getUsersByPhone(phone_nr);
            if(users!=null && users.size() > 0) {
                UserEntity userEntity = users.get(0);
                userEntity.setFirstName(first_name.getText().toString());
                userEntity.setLastName(last_name.getText().toString());
                userEntity.setBirthday(birth_day.getText().toString());
                userEntity.setEmail(email.getText().toString());
                userEntity.setGender(gender);

                if(new_password != null)
                {
                    userEntity.setPassword(new_password);
                }

                myAppDatabase.myDao().updateUserEntity(userEntity);
            }

            return users;
        }

        @Override
        protected void onPostExecute(List<UserEntity> users) {
            super.onPostExecute(users);

            Toast.makeText(getApplicationContext(), "Saved changes", Toast.LENGTH_SHORT).show();

        }
    };


    public interface FragmentProfileSettingsTaskDelegate{
        void onFinishedFragmentProfileSettingsTask();
    }

}
