package com.example.marinacernat.proiectdemo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

public class FillInProfileInformation extends AppCompatActivity {

    EditText firstName;
    EditText lastName;
    EditText email;
    Button register;
    CallbackManager callbackManager;
    LoginButton facebookButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fill_in_profile_information);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        callbackManager = CallbackManager.Factory.create();
        facebookButton = (LoginButton) findViewById(R.id.facebook_btn);

        List<String> permissionNeeds = Arrays.asList("public_profile", "email", "user_birthday", "user_gender");
        facebookButton.setReadPermissions(permissionNeeds);

        facebookButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                String userid = loginResult.getAccessToken().getUserId();
                GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        insertUserInfo(object);
                    }
                });


                Bundle parameters = new Bundle();
                parameters.putString("fields", "first_name, last_name, email, birthday, gender, id");
                graphRequest.setParameters(parameters);
                graphRequest.executeAsync();
            }

            @Override
            public void onCancel() {
                Toast.makeText(getApplicationContext(), "Login cancelled", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException exception) {

            }
        });

                register = (Button) findViewById(R.id.btn_register);

                register.setOnClickListener(new Button.OnClickListener(){

                  @Override
                public void onClick(View v) {

                Bundle SetPasswordData = getIntent().getExtras();
                if(SetPasswordData == null)
                {
                    return;
                }

                String phone = SetPasswordData.getString("phone");
                String password = SetPasswordData.getString("Password");

                firstName = (EditText) findViewById(R.id.et_FirstName);
                lastName = (EditText) findViewById(R.id.et_LastName);
                email = (EditText) findViewById(R.id.et_Email);

                UserEntity user = new UserEntity();

                user.setPhone(phone);
                user.setPassword(password);
                user.setFirstName(firstName.getText().toString());
                user.setLastName(lastName.getText().toString());
                user.setEmail(email.getText().toString());

                new insertIntoDB().execute(user);

             /*   SharedPreferences sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("logged", "true");
                editor.putString("user_phone", phone);
                editor.apply();  */
               }
        });
    }

    public void insertUserInfo(JSONObject object)
    {
        String first_name, last_name, e_mail, birth_day, gender, facebook_id;
        first_name = "";
        last_name = "";
        e_mail = "";
        facebook_id = "";
        birth_day= "";
        gender= "";

        try {

            first_name= object.getString("first_name");
            last_name = object.getString("last_name");
            e_mail = object.getString("email");
            facebook_id = object.getString("id");
            birth_day = object.getString("birthday");
            gender=object.getString("gender");



            Intent i = new Intent(FillInProfileInformation.this, Discover.class);
            i.putExtra("facebook_id", facebook_id);
            startActivity(i);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        Bundle SetPasswordData = getIntent().getExtras();
        if(SetPasswordData == null)
        {
            return;
        }

        String phone = SetPasswordData.getString("phone");
        String password = SetPasswordData.getString("Password");

        firstName = (EditText) findViewById(R.id.et_FirstName);
        lastName = (EditText) findViewById(R.id.et_LastName);
        email = (EditText) findViewById(R.id.et_Email);

        UserEntity user = new UserEntity();

        user.setPhone(phone);
        user.setPassword(password);
        user.setFirstName(first_name);
        user.setLastName(last_name);
        user.setEmail(e_mail);
        user.setFacebook_id(facebook_id);
        user.setBirthday(birth_day);
        user.setGender(gender);

        new insertIntoDB().execute(user);

      /*  SharedPreferences sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("logged", "true");
        editor.putString("user_phone", phone);
        editor.apply();  */

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    private class insertIntoDB extends AsyncTask<UserEntity, Void, String> {

        @Override
        protected String doInBackground(UserEntity...users) {

             MainActivity.myAppDatabase.myDao().addUserEntity(users[0]);

            //MainActivity.myAppDatabase.getDBInstance(getApplicationContext()).myDao().addUserEntity(users[0]);

            String succes = "Datele au fost inregistrate cu succes";
            return succes;
        }

        @Override
        protected void onPostExecute(String succes) {
            super.onPostExecute(succes);

            Toast.makeText(getApplicationContext(), succes, Toast.LENGTH_SHORT).show();
            Intent i = new Intent(getApplicationContext(), TestActivity.class);
            startActivity(i);
        }
    }
}
