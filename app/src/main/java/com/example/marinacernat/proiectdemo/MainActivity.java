package com.example.marinacernat.proiectdemo;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.arch.persistence.room.Room;
import android.widget.Toast;

import java.util.List;
import java.util.regex.Pattern;


public class MainActivity extends AppCompatActivity // implements TextView.OnEditorActionListener{
{
    EditText PhoneNumber;
    EditText Password;
    Button  SignIn, SignInTemporary, Skip;
    static MyAppDatabase myAppDatabase;
    int users_nr;
    private static final String TAG = "Continut db: ";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        PhoneNumber = (EditText) findViewById(R.id.et_PhoneNumber);
        Password = (EditText) findViewById(R.id.et_Password);
        SignIn = (Button) findViewById(R.id.SignIn_btn);
        SignInTemporary = (Button) findViewById(R.id.SignIn_btn_temporary);
        Skip = (Button) findViewById(R.id.SKIP_btn);

        myAppDatabase = Room.databaseBuilder(getApplicationContext(), MyAppDatabase.class, "userdb").build();

       // PhoneNumber.setOnEditorActionListener(this);

        Skip.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View v) {

                Intent i = new Intent(getApplicationContext(), Discover.class);
                startActivity(i);

                SharedPreferences sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("logged", "false");
                editor.apply();
            }
        });


        SignInTemporary.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View v) {
                if(PhoneNumber.getText().toString().length() != 10 || !Pattern.matches("[0-9]+", PhoneNumber.getText().toString()))
                {
                    alertView("Numarul nu are 10 cifre sau contine si alte caractere in afara de cifre.");

                }
                else
                {
                    new SearchAsyncTask(PhoneNumber.getText().toString(), new TaskDelegate() {
                        @Override
                        public void onFinishedTask() {
                            if (users_nr == 1)
                            {
                                SignInTemporary.setVisibility(View.GONE);
                                Password.setVisibility(View.VISIBLE);
                                SignIn.setVisibility(View.VISIBLE);

                                Button SignInButton = (Button) findViewById(R.id.SignIn_btn);

                                SignInButton.setOnClickListener(new Button.OnClickListener(){

                                    @Override
                                    public void onClick(View v) {

                                        new SearchPasswordAsyncTask(Password.getText().toString(), new TaskDelegate() {
                                            @Override
                                            public void onFinishedTask() {
                                                if (users_nr == 1)
                                                {
                                                    Intent i = new Intent(getApplicationContext(), Discover.class);
                                                    startActivity(i);

                                                    SharedPreferences sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                                    editor.putString("logged", "true");
                                                    editor.putString("user_phone", PhoneNumber.getText().toString());
                                                    editor.apply();
                                                }
                                                else
                                                if(users_nr == 0)
                                                {
                                                    Toast.makeText(getApplicationContext(), "Parola incorecta", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        }).execute();

                                    }
                                });
                           }
                           else
                            if(users_nr == 0)
                            {
                                Intent i = new Intent(MainActivity.this, SetPassword.class);
                                i.putExtra("phone", PhoneNumber.getText().toString());
                                startActivity(i);
                            }
                        }
                    }).execute();
               }
            }
        });


    }


     /*  @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
        {
            if (    actionId == EditorInfo.IME_ACTION_SEARCH ||
                    actionId == EditorInfo.IME_ACTION_NEXT ||
                    actionId == EditorInfo.IME_ACTION_DONE ||
                    event.getAction() == KeyEvent.ACTION_DOWN &&
                            event.getKeyCode() == KeyEvent.KEYCODE_ENTER  ) {

                            if (!event.isShiftPressed())
                            {

                              // if(PhoneNumber.getText().toString().length() != 10 || !Pattern.matches("[a-zA-Z]+", PhoneNumber.getText().toString()))
                                // DE FACUT SA MEARGA SI CU REGEX

                                if(PhoneNumber.getText().toString().length() != 10)
                                {
                                   alertView("Numarul nu are 10 cifre sau contine si alte caractere in afara de cifre.");

                                }
                                else
                                {
                                    new SearchAsyncTask(PhoneNumber.getText().toString(), new TaskDelegate() {
                                        @Override
                                        public void onFinishedTask() {
                                            if (users_nr == 1)
                                            {
                                                Password.setVisibility(View.VISIBLE);
                                                SignIn.setVisibility(View.VISIBLE);

                                                Button SignInButton = (Button) findViewById(R.id.SignIn_btn);

                                                SignInButton.setOnClickListener(new Button.OnClickListener(){

                                                    @Override
                                                    public void onClick(View v) {
                                                        Intent i = new Intent(getApplicationContext(), Discover.class);
                                                        startActivity(i);
                                                    }
                                                });
                                            }
                                            else
                                            if(users_nr == 0)
                                            {
                                                Intent i = new Intent(MainActivity.this, SetPassword.class);
                                                i.putExtra("phone", PhoneNumber.getText().toString());
                                                startActivity(i);
                                            }
                                        }
                                    }).execute();
                                }
                              return true; // consume.
                            }
            }
                        return false; // pass on to other listeners.
        }   */


    private void alertView( String message ) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog  .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

     public class SearchAsyncTask extends AsyncTask<Void, Void, List<UserEntity>> {

        String phone;
        TaskDelegate taskDelegate;

         SearchAsyncTask(String phone, TaskDelegate taskDelegate){
            this.phone=phone;
            this.taskDelegate = taskDelegate;
        }


        @Override
        protected List<UserEntity> doInBackground(Void... taskDelegates) {
            List<UserEntity> users = myAppDatabase.myDao().getUsersByPhone(phone);
            Log.d(TAG, phone);
            return users;
        }

        @Override
        protected void onPostExecute(List<UserEntity> users) {
            super.onPostExecute(users);
            users_nr = users.size();
            Log.d(TAG, Integer.toString(users_nr));
            if(taskDelegate!=null){
                taskDelegate.onFinishedTask();
            }
        }
    };


    public class SearchPasswordAsyncTask extends AsyncTask<Void, Void, List<UserEntity>> {

        String password;
        TaskDelegate taskDelegate;

        SearchPasswordAsyncTask(String password, TaskDelegate taskDelegate){
            this.password=password;
            this.taskDelegate = taskDelegate;
        }


        @Override
        protected List<UserEntity> doInBackground(Void... taskDelegates) {
            List<UserEntity> users = myAppDatabase.myDao().getUsersByPassword(password);
            Log.d(TAG, password);
            return users;
        }

        @Override
        protected void onPostExecute(List<UserEntity> users) {
            super.onPostExecute(users);
            users_nr = users.size();
            Log.d(TAG, Integer.toString(users_nr));
            if(taskDelegate!=null){
                taskDelegate.onFinishedTask();
            }
        }
    };

    public interface TaskDelegate{
        void onFinishedTask();
    }

}


