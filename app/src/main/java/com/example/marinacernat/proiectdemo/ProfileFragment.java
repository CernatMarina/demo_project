package com.example.marinacernat.proiectdemo;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import static com.example.marinacernat.proiectdemo.MainActivity.myAppDatabase;
import static com.facebook.FacebookSdk.getApplicationContext;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    View v;
    ImageView user_picture;
    String phone_number;
    String facebook_id;

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;

    ProfileSettings profileSettings;

    private Button settingsButton;

    private Button profile_back_button;

    public ProfileFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_fragment_profile, container, false);


        user_picture=(ImageView) v.findViewById(R.id.user_picture);
        settingsButton=(Button) v.findViewById(R.id.settings_button);
        profile_back_button=(Button) v.findViewById(R.id.profile_back_button);


        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences("userInfo", Context.MODE_PRIVATE);

        if(sharedPreferences.getString("logged", "").equals("true"))
        {
            Log.d("numar de tel",  sharedPreferences.getString("user_phone", ""));

            phone_number = sharedPreferences.getString("user_phone", "");

            new SearchUserAsyncTask(phone_number, new FragmentProfileTaskDelegate(){

                @Override
                public void onFinishedFragmentProfileTask() {

                    if(facebook_id != null)
                    {
                        new GetFacebookProfilePicture(new FragmentProfileTaskDelegate(){

                            @Override
                            public void onFinishedFragmentProfileTask() {

                                profile_back_button.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                                        fragmentTransaction.replace(R.id.main_frame, new HomeFragment()); // id-ul frame-layout-ului si id-ul fragmentului pe care vrem sa il punem
                                        fragmentTransaction.addToBackStack(null);
                                        fragmentTransaction.commit();
                                    }
                                });

                            }
                        }).execute();
                    }
                }
            }).execute();
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Nu sunteti logat", Toast.LENGTH_SHORT).show();
        }


        tabLayout = (TabLayout)v.findViewById(R.id.tablayout_id);
        viewPager = (ViewPager)v.findViewById(R.id.viewpager_id);

        viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());

        // Add Fragment Here
        viewPagerAdapter.AddFragment(new FragmentAll(), "ALL");
        viewPagerAdapter.AddFragment(new FragmentCoupons(), "COUPONS");
        viewPagerAdapter.AddFragment(new FragmentLastMinute(), "LAST MINUTE");
        viewPagerAdapter.AddFragment(new FragmentNew(), "NEW");

        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);


        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                profileSettings = new ProfileSettings();

                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.main_frame, profileSettings); // id-ul frame-layout-ului si id-ul fragmentului pe care vrem sa il punem
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });


        return v;

    }


    public class SearchUserAsyncTask extends AsyncTask<Void, Void, List<UserEntity>> {

        String phone;
        FragmentProfileTaskDelegate taskDelegate;

        SearchUserAsyncTask(String phone, FragmentProfileTaskDelegate taskDelegate){
            this.phone=phone;
            this.taskDelegate = taskDelegate;
        }


        @Override
        protected List<UserEntity> doInBackground(Void... taskDelegates) {
            List<UserEntity> users = myAppDatabase.myDao().getUsersByPhone(phone);

            return users;
        }

        @Override
        protected void onPostExecute(List<UserEntity> users) {
            super.onPostExecute(users);

            facebook_id = users.get(0).getFacebook_id();

            if(taskDelegate!=null){
                taskDelegate.onFinishedFragmentProfileTask();
            }
        }
    };


    public class GetFacebookProfilePicture extends AsyncTask<Void, Void, Bitmap> {

        FragmentProfileTaskDelegate taskDelegate;

        GetFacebookProfilePicture(FragmentProfileTaskDelegate taskDelegate){

            this.taskDelegate = taskDelegate;
        }


        @Override
        protected Bitmap doInBackground(Void... taskDelegates) {


            URL img_value = null;

            try {
                img_value = new URL("https://graph.facebook.com/"+facebook_id+"/picture?type=large");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            Bitmap mIcon1 = null;
            try {
                mIcon1 = BitmapFactory.decodeStream(img_value.openConnection().getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }

            return mIcon1;
        }

        @Override
        protected void onPostExecute(Bitmap mIcon1) {
            super.onPostExecute(mIcon1);

            user_picture.setImageBitmap(mIcon1);

            if(taskDelegate!=null){
                taskDelegate.onFinishedFragmentProfileTask();
            }
        }
    };


    public interface FragmentProfileTaskDelegate{
        void onFinishedFragmentProfileTask();
    }

}
