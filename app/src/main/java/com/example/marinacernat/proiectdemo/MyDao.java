package com.example.marinacernat.proiectdemo;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface MyDao   //DataAccessObject  - putem specifica operatiile pt baza de date (insert, update, delete, etc.) in interiorul acestei interfete
{
    // metoda pt inserarea unui utilizator in baza de date
    @Insert
    public void addUserEntity(UserEntity user);

    @Query("select * from users")
    public List<UserEntity> getUsers();

    @Query("SELECT * FROM users WHERE user_phone == :phone")
    public abstract List<UserEntity> getUsersByPhone(String phone);

    @Query("SELECT * FROM users WHERE password == :password")
    public abstract List<UserEntity> getUsersByPassword(String password);

    @Update
    public void updateUserEntity(UserEntity user);
}
