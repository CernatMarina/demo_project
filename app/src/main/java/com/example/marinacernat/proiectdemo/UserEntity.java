package com.example.marinacernat.proiectdemo;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "users")
public class UserEntity {
    public UserEntity() {
    }

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "user_phone")
    // folosim asta pentru a customiza numele coloanei, altfel, ele au numele campului
    private String phone;

    @ColumnInfo(name = "password")
    private String password;

    @ColumnInfo(name = "user_firstName")
    // folosim asta pentru a customiza numele coloanei, altfel, ele au numele campului
    private String firstName;

    @ColumnInfo(name = "user_lastName")
    // folosim asta pentru a customiza numele coloanei, altfel, ele au numele campului
    private String lastName;

    @ColumnInfo(name = "user_email")
    private String email;

    @ColumnInfo(name = "birth_day")
    private String birthday;

    @ColumnInfo(name = "gender")
    private String gender;

    @ColumnInfo(name = "facebook_user_id")
    private String facebook_id;


    public void setId(int id) {
        this.id = id;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setFacebook_id(String facebook_id) {
        this.facebook_id = facebook_id;
    }

    public int getId() {
        return id;
    }

    public String getPassword() {
        return password;
    }

    public String getPhone() {
        return phone;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getBirthday() {
        return birthday;
    }

    public String getGender() {
        return gender;
    }

    public String getFacebook_id() {
        return facebook_id;
    }
}
