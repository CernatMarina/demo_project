package com.example.marinacernat.proiectdemo;


import android.os.Bundle;

import android.os.Parcelable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.marinacernat.proiectdemo.api.model.Movie;
import com.example.marinacernat.proiectdemo.api.response.GetMoviesResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    View v;

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_home, container, false);


        tabLayout = (TabLayout)v.findViewById(R.id.tablayout_id);
        viewPager = (ViewPager)v.findViewById(R.id.viewpager_id);

        viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());


        // Add Fragment Here
        viewPagerAdapter.AddFragment(new FragmentAll(), "ALL");
        viewPagerAdapter.AddFragment(new FragmentCoupons(), "COUPONS");
        viewPagerAdapter.AddFragment(new FragmentLastMinute(), "LAST MINUTE");
        viewPagerAdapter.AddFragment(new FragmentNew(), "NEW");


        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        return v;
    }


}
