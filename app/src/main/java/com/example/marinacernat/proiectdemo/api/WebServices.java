package com.example.marinacernat.proiectdemo.api;

import com.example.marinacernat.proiectdemo.api.response.GetMoviesResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface WebServices {

    @GET("3/discover/movie")
    public Call<GetMoviesResponse> getMovies(@Query("api_key") String apiKeyStr);

}

