package com.example.marinacernat.proiectdemo;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class TestActivity extends AppCompatActivity {
    private TextView TxtInfo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        TxtInfo = (TextView) findViewById(R.id.test_textView);

        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, List<UserEntity>> getTask = new AsyncTask<Void, Void, List<UserEntity>>() {
            @Override
            protected List<UserEntity> doInBackground(Void...voids) {
                List<UserEntity> users = MainActivity.myAppDatabase.myDao().getUsers();
                return users;
            }

            @Override
            protected void onPostExecute(List<UserEntity> users) {
                super.onPostExecute(users);

                String info = " ";
                for(UserEntity usr : users)
                {
                    int id = usr.getId();
                    String name = usr.getFirstName();
                    String lastName = usr.getLastName();
                    String phoneNumber = usr.getPhone();
                    String email = usr.getEmail();
                    String password = usr.getPassword();
                    String birthday = usr.getBirthday();
                    String gender = usr.getGender();
                    String facebook_id = usr.getFacebook_id();

                    info = info + "  " + "Id: " + id + "  " +
                            "firstName: " + name + "  " +
                            "lastName: " + lastName + "  "+
                            "phoneNumber: " + phoneNumber + "  "+
                            "Email: " + email +
                            "password: " + password + "   " +
                            "birthday: " + birthday + "   " +
                            "gender: " + gender + "   " +
                            "id: " + facebook_id;
                    TxtInfo.setText(info);


                }

            }
        };
        getTask.execute();

    }


}
