package com.example.marinacernat.proiectdemo;

//Este o clasa abstracta care extinde RoomDatabase, asta e baza de date
// Include lista de entitati asociate bazei de date in interiorul anotarii
// Contine o metoda abstracta care are 0 argumente si returneaza clasa care este anotata cu dao

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = {UserEntity.class}, version = 4, exportSchema = false)  // entities adica tabelele, in cazul nostru doar unul
public abstract class MyAppDatabase extends RoomDatabase
{
    //public abstract MyDao daoEntity(); - de sters

    public static final String DATABASE_NAME = "Room_Database";

    private static MyAppDatabase dbInstance;

    public static MyAppDatabase getDBInstance(Context context){
        if(dbInstance == null){
            dbInstance = Room.databaseBuilder(context, MyAppDatabase.class, DATABASE_NAME)
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return dbInstance;
    }

    public abstract MyDao myDao();
}


// clasa abstracta abstract class MyAppDatabase reprezinta baza de date si contine
// metoda abstracta abstract MyDao myDao() ce returneaza un obiect de tip MyDao ce este obligat sa aiba metodele din interfata MyDao, fiind de tipul ei
// MyDao este o interfata care va contine toate metodele ce implementeaza operatiile pe baza de date

// practic avem baza de date si operatiile ei
