package com.example.marinacernat.proiectdemo;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.marinacernat.proiectdemo.api.model.Movie;
import com.example.marinacernat.proiectdemo.api.response.GetMoviesResponse;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.transform.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.CAMERA;
import static com.facebook.FacebookSdk.getApplicationContext;

public class Discover extends AppCompatActivity  implements ZXingScannerView.ResultHandler{

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;

    private BottomNavigationView mMainNav;
    private FrameLayout mMainFrame;



    private HomeFragment homeFragment;
    private MapFragment mapFragment;
    private ProfileFragment profileFragment;

    private static final int REQUEST_CAMERA = 1;
    private ZXingScannerView scannerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discover);


        mMainNav = (BottomNavigationView) findViewById(R.id.navigation);

        homeFragment = new HomeFragment();
        mapFragment = new MapFragment();
        profileFragment = new ProfileFragment();

     /*   Bundle setProfilePicture = getIntent().getExtras();
        if(setProfilePicture == null)
        {
            return;
        }

        String facebook_id = setProfilePicture.getString("facebook_id");

        Bundle bundle = new Bundle();
        bundle.putString("facebook_id", "facebook_id");
        profileFragment.setArguments(bundle);  */



        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_frame, homeFragment); // id-ul frame-layout-ului si id-ul fragmentului pe care vrem sa il punem
        fragmentTransaction.commit();

        mMainNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        setFragment(homeFragment);
                        return true;

                    case R.id.navigation_points:
                        // setFragment(pointsFragment);

                        scannerView = new ZXingScannerView(getApplication());
                        setContentView(scannerView);
                        scannerView.setResultHandler(Discover.this);
                        scannerView.startCamera();


                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                        {
                            // va verifica daca avem sau nu permisiune
                            if(checkPermission())
                            {
                                Toast.makeText(Discover.this, "Permission is granted!", Toast.LENGTH_LONG).show();
                            }
                            else
                            {
                                requestPermission();
                            }
                        }
                        return true;

                    case R.id.navigation_map:
                        setFragment(mapFragment);
                        return true;

                    case R.id.navigation_profile:
                       //   item.setIcon(R.drawable.ic_home);

                        SharedPreferences sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE);

                        if(sharedPreferences.getString("logged", "").equals("true"))
                        {
                            setFragment(profileFragment);
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(), "Nu sunteti logat", Toast.LENGTH_SHORT).show();
                        }

                    default:
                        return false;
                }
            }
        });

    }

    private boolean checkPermission()  // va returna adevarat sau fals daca avem sau nu permisiuni
    {
        return (ContextCompat.checkSelfPermission(Discover.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED);
    }


    private void requestPermission() // doar va cere permisiuni, nu va returna nimic
    {
        ActivityCompat.requestPermissions(this, new String[]{CAMERA}, REQUEST_CAMERA);  // requestPermissions() are trei parametrii: activitatea, permisiunile - care sunt un array de string-uri, si un cod de request

    }

    public void onRequestPermissionsResult(int requestCode, String permission[], int grantResults[])
    {
        switch(requestCode)
        {
            case REQUEST_CAMERA:
                if(grantResults.length > 0)
                {
                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if(cameraAccepted)
                    {
                        Toast.makeText(Discover.this, "Permission Granted", Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        Toast.makeText(Discover.this, "Permission Denied", Toast.LENGTH_LONG).show();
                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                        {
                            if(shouldShowRequestPermissionRationale(CAMERA))
                            {
                                    displayAlertMessage("You need to allow acces for both permissions", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                                            {
                                                requestPermissions(new String[]{CAMERA}, REQUEST_CAMERA);
                                            }
                                        }
                                    });
                               return;
                            }
                        }
                    }
                }

            break;
        }
    }


    @Override
    public void onDestroy()
    {
        super.onDestroy();
        scannerView.stopCamera();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if(checkPermission())
            {
                if(scannerView == null)
                {
                    scannerView = new ZXingScannerView(this);
                    setContentView(scannerView);
                }
                scannerView.setResultHandler(this);
                scannerView.startCamera();

            }
            else
            {
                requestPermission();
            }
        }
    }


    public void displayAlertMessage(String message, DialogInterface.OnClickListener listener)
    {
        new AlertDialog.Builder(Discover.this)
                .setMessage(message)
                .setPositiveButton("OK", listener)  // cand apasam pe ok, va cere permisiuni, de aceea avem aici listenre
                .setNegativeButton("Cancel", null)  // cand apasam pe cancel, nu face nimic
                .create()
                .show();

    }


    public void handleResult(com.google.zxing.Result result) {

        final String scanResult = result.getText();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Scan Result");

        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                scannerView.resumeCameraPreview(Discover.this);

            }
        });

        builder.setNeutralButton("Visit", new DialogInterface.OnClickListener(){

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(scanResult));
                startActivity(intent);
            }
        });

        builder.setMessage(scanResult);
        AlertDialog alert = builder.create();
        alert.show();

    }


    private void setFragment(Fragment fragment)
    {

            // pentru a inlocui fragmentul curent cu cel primit ca parametru

            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.main_frame, fragment); // id-ul frame-layout-ului si id-ul fragmentului pe care vrem sa il punem
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
    }


}
