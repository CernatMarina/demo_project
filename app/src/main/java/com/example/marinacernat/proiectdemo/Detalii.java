package com.example.marinacernat.proiectdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import static com.facebook.FacebookSdk.getApplicationContext;

public class Detalii extends AppCompatActivity {

    private TextView title;
    private ImageView poster_path;
    private TextView overview;
    private TextView vote_count;
    private String title_intent;
    private String poster_path_intent;
    private String overview_intent;
    private  String vote_count_intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalii_oferte);

        Bundle ItemType = getIntent().getExtras();
        if( ItemType == null)
        {
            return;
        }

        else
        {
            title_intent = ItemType.getString("title");
            poster_path_intent = ItemType.getString("poster_path");
            overview_intent = ItemType.getString("overview");
            vote_count_intent = ItemType.getString("vote_count");
        }

        title = (TextView) findViewById(R.id.movie_title);
        poster_path = (ImageView) findViewById(R.id.item_poster_path);
        overview = (TextView) findViewById(R.id.item_overview);
        vote_count = (TextView) findViewById(R.id.item_vote_count);

        title.setText(title_intent);

        Picasso.with(getApplicationContext()).load(poster_path_intent).into(poster_path);

        overview.setText(overview_intent);

        vote_count.setText(vote_count_intent);


    }
}
