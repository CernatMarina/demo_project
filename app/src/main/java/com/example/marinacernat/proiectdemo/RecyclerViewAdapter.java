package com.example.marinacernat.proiectdemo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.marinacernat.proiectdemo.api.model.Movie;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static com.facebook.FacebookSdk.getApplicationContext;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    List<Movie> mData;
    private final static int VIEW_HOLDER_BIG = 2;
    private final static int VIEW_HOLDER_LITTLE = 1;
    private Context context;
    FragmentManager fm;
    DetaliiOferte detaliiOferte;


    // private final View.OnClickListener mOnClickListener = new MyOnClickListener();

    public RecyclerViewAdapter(Context mContext, FragmentManager fm) {
        this.mContext = mContext;

        this.mData = new ArrayList<>();

        this.fm = fm;

    }

    public void updateMoviesList(List<Movie> movies)
    {
        mData.clear();
        mData.addAll(movies);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if(position%3 == 0)
            return VIEW_HOLDER_BIG;
        else
            return VIEW_HOLDER_LITTLE;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;


        switch (viewType) {
            case VIEW_HOLDER_BIG:
                    v = LayoutInflater.from(mContext).inflate(R.layout.item_all,parent,false);
                    final MyViewHolder myViewHolder = new MyViewHolder(v);

                    myViewHolder.item_all.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View v)
                        {

                           Bundle args = new Bundle();
                           args.putString("title", myViewHolder.title.getText().toString());
                           args.putString("poster_path", myViewHolder.image_url);
                           args.putString("overview", myViewHolder.overview.getText().toString());
                           args.putString("vote_count", myViewHolder.vote_count.getText().toString());


                           detaliiOferte = new DetaliiOferte();
                           detaliiOferte.setArguments(args);

                           FragmentTransaction fragmentTransaction = fm.beginTransaction();
                           fragmentTransaction.replace(R.id.main_frame, detaliiOferte); // id-ul frame-layout-ului si id-ul fragmentului pe care vrem sa il punem
                           fragmentTransaction.addToBackStack(null);
                           fragmentTransaction.commit();

                          // Toast.makeText(getApplicationContext(), "Test Click"+String.valueOf(myViewHolder.getAdapterPosition()), Toast.LENGTH_SHORT).show();
                        }
                    });

                    return myViewHolder;
            default:
                    v = LayoutInflater.from(mContext).inflate(R.layout.little_item_all,parent,false);
                    final MyLittleViewHolder myLittleViewHolder = new MyLittleViewHolder(v);

                    myLittleViewHolder.little_item_all.setOnClickListener(new View.OnClickListener(){
                        @Override
                         public void onClick(View v)
                        {

                            Bundle args = new Bundle();
                            args.putString("title", myLittleViewHolder.little_title.getText().toString());
                            args.putString("poster_path", myLittleViewHolder.image_url);
                            args.putString("overview", myLittleViewHolder.little_overview);
                            args.putString("vote_count", myLittleViewHolder.little_vote_count.getText().toString());

                            detaliiOferte = new DetaliiOferte();
                            detaliiOferte.setArguments(args);

                            FragmentTransaction fragmentTransaction = fm.beginTransaction();
                            fragmentTransaction.replace(R.id.main_frame, detaliiOferte); // id-ul frame-layout-ului si id-ul fragmentului pe care vrem sa il punem
                            fragmentTransaction.addToBackStack(null);
                            fragmentTransaction.commit();

                           // Toast.makeText(getApplicationContext(), "Test Click"+String.valueOf(myLittleViewHolder.getAdapterPosition()), Toast.LENGTH_SHORT).show();
                        }
                    });

                return myLittleViewHolder;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {


        switch (holder.getItemViewType()) {
            case VIEW_HOLDER_BIG:

                MyViewHolder myViewHolder = (MyViewHolder)holder;

                // facem conexiunea intre layout-ul unui item si datele din obiectul item-ului, de pe pozitia position in lista mData

                myViewHolder.title.setText(mData.get(position).getTitle());

                String url = "https://image.tmdb.org/t/p/w500/" + mData.get(position).getPosterPath();

                Picasso.with(getApplicationContext()).load(url).into(myViewHolder.poster_path);
                myViewHolder.image_url = url;

                myViewHolder.overview.setText(mData.get(position).getOverview());

                myViewHolder.vote_count.setText(mData.get(position).getVoteCount() + "");



                break;

            default:
                MyLittleViewHolder myLittleViewHolder = (MyLittleViewHolder)holder;

                // facem conexiunea intre layout-ul unui item si datele din obiectul item-ului, de pe pozitia position in lista mData

                myLittleViewHolder.little_title.setText(mData.get(position).getTitle());

                String url2 = "https://image.tmdb.org/t/p/w500/" + mData.get(position).getPosterPath();

                Picasso.with(getApplicationContext()).load(url2).into(myLittleViewHolder.little_poster_path);

                myLittleViewHolder.image_url = url2;

                myLittleViewHolder.little_overview = mData.get(position).getOverview();

                myLittleViewHolder.little_vote_count.setText(mData.get(position).getVoteCount() + "");

                break;
        }


    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public  class MyViewHolder extends RecyclerView.ViewHolder{

        private LinearLayout item_all;
        private TextView title;
        private ImageView poster_path;
        private String image_url;
        private TextView overview;
        private TextView vote_count;

        public MyViewHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            item_all = (LinearLayout) itemView.findViewById(R.id.item_all_id);
            title = (TextView) itemView.findViewById(R.id.movie_title);
            poster_path = (ImageView) itemView.findViewById(R.id.item_poster_path);
            overview = (TextView) itemView.findViewById(R.id.item_overview);
            vote_count = (TextView) itemView.findViewById(R.id.item_vote_count);
        }
    }


    public class MyLittleViewHolder extends RecyclerView.ViewHolder{

        private LinearLayout little_item_all;
        private TextView little_title;
        private ImageView little_poster_path;
        private String image_url;
        private String little_overview;
        private TextView little_vote_count;

        public MyLittleViewHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            little_item_all = (LinearLayout) itemView.findViewById(R.id.little_item_all_id);
            little_title = (TextView) itemView.findViewById(R.id.little_movie_title);
            little_poster_path = (ImageView) itemView.findViewById(R.id.litte_item_poster_path);
            little_vote_count = (TextView) itemView.findViewById(R.id.little_item_vote_count);
        }
    }





}
