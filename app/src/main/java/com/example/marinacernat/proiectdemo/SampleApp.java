package com.example.marinacernat.proiectdemo;

import android.app.Application;

import com.example.marinacernat.proiectdemo.api.ApiClient;
import com.example.marinacernat.proiectdemo.api.WebServices;


public class SampleApp extends Application {

    private WebServices webServices;

    @Override
    public void onCreate() {
        super.onCreate();
        webServices = ApiClient.getWebServices(getString(R.string.base_url));
    }

    public WebServices getWebServices() {
        return webServices;
    }
}